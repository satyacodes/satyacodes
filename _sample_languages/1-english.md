---
title: English
permalink: "/languages/english.html"
key: lang-en
cover: "/assets/java.png"
lang: en
---

English.

<!--more-->

*_config.yml* or front matter:

```yml
lang: en
lang: en-GB
lang: en-US
lang: en-CA
lang: en-AU
```