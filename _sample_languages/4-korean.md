---
title: 한국어 (Korean)
permalink: "/languages/korean.html"
key: lang-ko
cover: "/assets/java.png"
lang: ko
---

한국어.

<!--more-->

*_config.yml* or front matter:

```yml
lang: ko
lang: ko-KR
```