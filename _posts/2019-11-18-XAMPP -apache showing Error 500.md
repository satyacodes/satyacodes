---
title: XAMPP - Remove .htaccess file
date: 2019-011-11 00:00:00 Z
categories:
- XAMPP
tags:
- XAMPP
layout: article
thumbnail: "/assets/xampp.jpg"
sharing: true
license: true
aside:
  toc: true
pageview: true
---
Remove .htaccess file