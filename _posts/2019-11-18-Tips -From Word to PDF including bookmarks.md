---
title: How to - Create book from Word to PDF including bookmarks
date: 2019-11-11 00:00:00 Z
categories:
- How to
tags:
- How to
layout: article
thumbnail: "/assets/tips.jpg"
sharing: true
license: true
aside:
  toc: true
pageview: true
---


How to create book from Word to PDF including bookmarks

This is easy to do in Word 2007 and 2010. You don't need any third party tools.

In Word 2007, you need the Microsoft PDF Add-In to allow you to save as PDF.
Download it
from[here](http://www.microsoft.com/downloads/details.aspx?FamilyID=f1fc413c-6d89-4f15-991b-63b07ba5f2e5&displaylang=en).
In Word 2010 the Add-In is unnecessary.



1.Open your document



2.Mark the headings you want using the standard Heading 1, Heading 2, etc.
styles.



3.Select **Save As \> PDF**



4.The first time you do this you have to set an option **\>ClickOptions**

![http://localhost:6666/sml/wp-content/uploads/2017/09/w1.png](media/b08d774a03c1f0909c8b22fd23ba6136.png)



5.Check the**Create Bookmarks using headings**check box.

![](media/674e0b48b499e58a9bd4f42769fb9bef.png)



6.Click OK & Publish
